import React, { Component, Fragment as RF } from 'react';
import "./index.scss";
import Spinner from "/components/Spinner";
import AudioTrack from "/components/AudioTrack";

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      step: 1,
      res: undefined
    };
  }

  async componentDidMount() {
    const res = await fetch('/api', { method: 'GET'});
    const json = await res.json();
    this.setState({ res: json.data });
  }

  render() {
    const { step, res } = this.state;

    return (
      <div>
        <div>This is step n. {step}</div>
        <AudioTrack />
        <div>
          <p>Api Says:</p>
          {
            res || 'nothing'
          }
        </div>
      </div>
    );
  }
}

export default App;