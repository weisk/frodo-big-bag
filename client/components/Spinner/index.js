import react from "react";
import { makeStyles } from '@material-ui/core/styles';
import {
  Loop as LoopIcon
} from '@material-ui/icons';



const useStyles = makeStyles((theme) => ({
  spinning: {
    animation: '$spinning .5s infinite alternate',
  },
  '@keyframes spinning': {
    '0%': { transform: 'rotate(0)' },
    '100%': { transform: 'rotate(180deg)' },
  },
}));

export default function Spinner() {
  const classes = useStyles();
  return <LoopIcon className={classes.spinning} />
}