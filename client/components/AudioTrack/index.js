import React, { Component } from "react";

const TRACKS = [
  '/public/blissful.mp3',
  '/public/epic.mp3',
  '/public/scary.mp3',
];

export default class AudioTrack extends Component {
  constructor(props) {
    super(props);
    this.state = {};
    this.loadTracks(TRACKS);
  }

  async loadTracks(tracks) {
    let trackPromises = TRACKS.map((track) => {
      return new Promise((res, rej) => {
        let audioTrack = new Audio(track);
        audioTrack.addEventListener('canplaythrough', () => {
          console.log('But i Can!');
          res(audioTrack);
        });
      });
    });
    const [...allTracks] = await Promise.all(trackPromises)
    console.log(allTracks);
  }

  render() {
    return <div>Not sure</div>;
  }

}