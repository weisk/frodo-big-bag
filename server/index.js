import express from 'express';
import bodyParser from 'body-parser';
import logger from 'morgan';
import router from './router.js';

const app = express();

app.use(logger('dev'))

app.use(bodyParser.json());

app.use(router);

const port = process.env.PORT || 8000;
app.listen(port, '0.0.0.0', () => {
  console.log(`Listening on port ${port}...`);
});