const ILLEGAL_MOVE = {
  message: 'You can only move north, west, east, or south at each time'
};

const FALL_MOVE = {
  message: 'You fell into the void'
};

export {
  ILLEGAL_MOVE,
  FALL_MOVE,
};
