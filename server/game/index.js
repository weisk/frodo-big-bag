import Game from './Game.js';
import { v4 as uuidv4 } from 'uuid';

let GAMES = {};

export function Start() {
  const id = uuidv4().replace(/-/,'');
  const game = new Game();
  GAMES[id] = game;
  return id;
}