import * as Actions from './Actions.js';
import * as Constants from './Constants.js';

export default class Game {
  constructor() {
    this.grid = INITIAL_GRID;
    this.position = INITIAL_POSITION;
  }

  moveTo(direction) {
    if (!Constants.MOVEMENTS.includes(direction)) {
      return Actions.ILLEGAL_MOVE;
    }

    const nextCell = this.getNextCell(direction);
  }

  getNextCell(direction) {
    const {
      grid,
      position: { x, y }
    } = this;

    let x1, y1;

    switch(direction) {
      case 'w': x1 = x-1; break;
      case 'e': x1 = x+1; break;
      case 's': y1 = y-1; break;
      case 'n': y1 = y+1; break;
    }

    if (x1 < 0 || x1 >= grid[0].length) {
      return Actions.FALL_MOVE;
    }

  }
}