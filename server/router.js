import express from 'express';
import * as Handlers from './handlers.js';
import * as Constants from './Constants.js';

const router = express.Router();

router.get('/', (req, res) => {
  res.status(200).json({ data: 'all is good', errors: [] });
});

router.post('/game', (req, res) => {
  const result = Handlers.startGame();
  res.status(200).json(result);
});

router.put('/game/:gameId/', (req, res) => {
  const result = Handlers.checkGame();
  if (result) {
    res.status(200).json(result);
  } else {
    res.status(404).json({ data: '', errors: ['Game not found'] });
  }
});

router.put('/game/:gameId/move', (req, res) => {
  const { gameId } = req.params;
  const { direction } = req.body;
  if (!gameId || !Constants.MOVEMENTS.includes(direction)) {
    return res.status(400);
  }
  const result = Handlers.gameMove(gameId, direction);
  res.status(200).json(result);
});

export default router;