plan.md

generate scaffold for
  - spa
  - rest api
  - development hot-reload

design data structure to represent the game

implement game logic on the server

implement interaction between client and server

implement UI